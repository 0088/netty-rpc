package org.levi.service;

/**
 * @author DevCenter
 */
public interface HelloService {

    void sayHello(String name);
}
