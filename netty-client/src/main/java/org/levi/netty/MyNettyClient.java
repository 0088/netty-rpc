package org.levi.netty;

import org.levi.service.HelloService;

public class MyNettyClient {

    public static void main(String[] args) throws Exception {

        HelloService helloService = (HelloService) RPCConsumer.createProxy(HelloService.class);
        while (true) {
            helloService.sayHello("Nicole Fu");
            Thread.sleep(5000);
        }
    }
}
