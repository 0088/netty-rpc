package org.levi.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author DevCenter
 */
public class TimeClientHandler extends ChannelInboundHandlerAdapter {


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf m = (ByteBuf) msg;
        try {
            for (int i = 0; i < m.capacity(); i++) {
                byte b = m.getByte(i);
                System.out.print((char) b);
            }
        } finally {
            m.release();
        }
    }
}
