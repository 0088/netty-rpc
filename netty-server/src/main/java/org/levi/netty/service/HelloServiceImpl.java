package org.levi.netty.service;

import org.levi.service.HelloService;

/**
 * @author DevCenter
 */
public class HelloServiceImpl implements HelloService {


    public void sayHello(String name) {
        System.out.println("hello " + name + " I am netty!");
    }
}
